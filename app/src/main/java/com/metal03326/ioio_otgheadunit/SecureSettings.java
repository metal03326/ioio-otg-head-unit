package com.metal03326.ioio_otgheadunit;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;

public class SecureSettings extends Fragment {
    View layout;
    SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_secure_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layout = getView();
        preferences = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);

        RecyclerView secureSwitchesRecyclerView = layout.findViewWithTag("secureSwitches");

        String[][] secureSwitches = {
                {"immersiveMode", getString(R.string.immersive_mode), getString(R.string.immersive_mode_help)},
                {"volumeSuppression", getString(R.string.volume_suppression), getString(R.string.volume_suppression_help)}
        };

        // Android 11 no longer allows system wide immersive mode :(
        if (Build.VERSION.SDK_INT >= 30) {
            secureSwitches = Arrays.copyOfRange(secureSwitches, 1, secureSwitches.length);
        }

        secureSwitchesRecyclerView.setHasFixedSize(true);
        secureSwitchesRecyclerView.setAdapter(new SwitchItemAdapter(secureSwitches, preferences, this::setSecureSettings));

        setSecureSettings();
    }

    private void setSecureSettings() {
        // Set visibility of the device admin fab
        final boolean canWriteSecureSettings = requireActivity().checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") == PackageManager.PERMISSION_GRANTED;
        boolean immersiveMode = false;

        // In case user did something wrong with boolean value in Expert mode
        try {
            immersiveMode = preferences.getBoolean("immersiveMode", false);
        } catch (Exception ignored) {
        }

        if (canWriteSecureSettings) {
            String value = "null*";

            if (immersiveMode) {
                value = "immersive.full=*";
            }

            Settings.Global.putString(requireActivity().getContentResolver(), "policy_control", value);
        }

        // In case user did something wrong with boolean value in Expert mode
        try {
            layout.findViewWithTag("noWriteSecureSettingsPermission").setVisibility(!canWriteSecureSettings && (immersiveMode || preferences.getBoolean("volumeSuppression", false)) ? View.VISIBLE : View.GONE);
        } catch (Exception ignored) {
        }
    }
}