package com.metal03326.ioio_otgheadunit;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.switchmaterial.SwitchMaterial;

public class SwitchItemAdapter extends RecyclerView.Adapter<SwitchItemAdapter.ViewHolder> {
    private final String[][] localDataSet;
    private final SharedPreferences preferences;
    private final Runnable runValidations;

    private void onHelpClick(View view) {
        final String[] switchItem = localDataSet[Integer.parseInt(view.getTag().toString())];

        new AlertDialog.Builder(view.getContext())
                .setTitle(switchItem[1])
                .setMessage(Html.fromHtml(switchItem[2], Html.FROM_HTML_MODE_LEGACY))
                .setPositiveButton(android.R.string.ok, null).show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final SwitchMaterial switchMaterial;
        private final FloatingActionButton helpButton;

        public ViewHolder(View view) {
            super(view);

            switchMaterial = view.findViewWithTag("switch");
            helpButton = view.findViewWithTag("help");
        }

        public SwitchMaterial getSwitchMaterial() {
            return switchMaterial;
        }

        public FloatingActionButton getHelpButton() {
            return helpButton;
        }
    }

    public SwitchItemAdapter(final String[][] dataSet, final SharedPreferences prefs, final Runnable validator) {
        localDataSet = dataSet;
        preferences = prefs;
        runValidations = validator;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.switch_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int index) {
        final String[] switchItem = localDataSet[index];
        final SwitchMaterial switchMaterial = viewHolder.getSwitchMaterial();
        final FloatingActionButton helpButton = viewHolder.getHelpButton();
        final String tag = switchItem[0];

        switchMaterial.setTag(tag);
        switchMaterial.setText(switchItem[1]);

        helpButton.setTag(index);

        // In case user did something wrong with boolean value in Expert mode
        try {
            switchMaterial.setChecked(preferences.getBoolean(tag, false));
        } catch (Exception ignored) {
        }

        switchMaterial.setOnCheckedChangeListener((view, isChecked) -> {
            preferences.edit().putBoolean(String.valueOf(view.getTag()), isChecked).apply();

            new Handler().post(runValidations);
        });
        helpButton.setOnClickListener(this::onHelpClick);
    }

    @Override
    public int getItemCount() {
        return localDataSet.length;
    }
}
