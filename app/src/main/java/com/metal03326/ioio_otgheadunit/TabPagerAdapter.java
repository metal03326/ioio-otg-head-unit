package com.metal03326.ioio_otgheadunit;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class TabPagerAdapter extends FragmentStateAdapter {
    int count;

    public TabPagerAdapter(FragmentActivity fa, int length) {
        super(fa);
        this.count = length;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        switch (position) {
            case 1:
                return new SecureSettings();
            case 2:
                return new StartupSettings();
            case 3:
                return new IoioSettings();
            case 4:
                return new SteeringWheelButtonsFragment();
            case 5:
                return new DebugInformationFragment();
            case 0:
            default:
                return new GeneralSettings();
        }
    }

    @Override
    public int getItemCount() {
        return count;
    }
}
