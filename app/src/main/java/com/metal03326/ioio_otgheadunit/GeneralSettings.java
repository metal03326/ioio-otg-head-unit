package com.metal03326.ioio_otgheadunit;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class GeneralSettings extends Fragment {
    View layout;
    SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_general_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layout = getView();
        preferences = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);

        RecyclerView generalSwitchesRecyclerView = layout.findViewWithTag("generalSwitches");

        String[][] generalSwitches = {
                {"showAudioVolume", getString(R.string.show_audio_volume_on_change), getString(R.string.show_audio_volume_on_change_help)},
                {"beepOnVolumeChange", getString(R.string.beep_on_audio_volume_change), getString(R.string.beep_on_audio_volume_change_help)},
                {"longBeepOnMaxVolume", getString(R.string.long_beep_on_max_volume), getString(R.string.long_beep_on_max_volume_help)},
                {"beepOnMultimediaAction", getString(R.string.beep_on_multimedia_action), getString(R.string.beep_on_multimedia_action_help)},
                {"beepOnGlobalAction", getString(R.string.beep_on_global_action), getString(R.string.beep_on_global_action_help)},
                {"beepOnAppLaunch", getString(R.string.beep_on_app_launch), getString(R.string.beep_on_app_launch_help)},
                {"briefWakeOnVolumeChange", getString(R.string.brief_screen_awake_on_volume_change), getString(R.string.brief_screen_awake_on_volume_change_help)},
                {"briefWakeOnTrackChange", getString(R.string.brief_screen_awake_on_multimedia_action), getString(R.string.brief_screen_awake_on_multimedia_action_help)},
                {"briefShowNotificationsTrackChange", getString(R.string.brief_show_notifications_on_track_change), getString(R.string.brief_show_notifications_on_track_change_help)}
        };

        generalSwitchesRecyclerView.setHasFixedSize(true);

        generalSwitchesRecyclerView.setAdapter(new SwitchItemAdapter(generalSwitches, preferences, new Runnable() {
            @Override
            public void run() {

            }
        }));
    }
}