package com.metal03326.ioio_otgheadunit;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;

public class CustomErrorActivity extends AppCompatActivity {
    private ActivityResultLauncher<Intent> expertModeActivityResultLauncher;

    @SuppressLint("ApplySharedPref")
    static void updatePreferencesAndRestart(final ActivityResult result, final Context context) {
        if (result.getResultCode() == AppCompatActivity.RESULT_OK && result.getData() != null) {
            final String jsonString = Objects.requireNonNull(result.getData().getStringExtra("result"));
            final SharedPreferences.Editor editor = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).edit();

            // Remove all entries. This is a shortcut to remove items if user removed them from JSON
            editor.clear();

            try {
                final JSONObject json = new JSONObject(jsonString);
                final Iterator<String> iterator = json.keys();

                while (iterator.hasNext()) {
                    final String key = iterator.next();

                    try {
                        final String value = json.get(key).toString();

                        if (value.equals("true") || value.equals("false")) {
                            editor.putBoolean(key, Boolean.parseBoolean(value));
                        } else {
                            try {
                                editor.putInt(key, Integer.parseInt(value));
                            } catch (Exception ignored) {
                                editor.putString(key, value);
                            }
                        }
                    } catch (Exception ignored) {
                    }
                }
            } catch (Exception ignored) {
            }

            // Save changes. apply() is not working here - we exit the process in a bit
            editor.commit();

            // Stop service
            context.stopService(new Intent(context, IOIOHUService.class));

            // Restart app
            context.startActivity(Intent.makeRestartActivityTask(context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()).getComponent()));
            System.exit(0);
        }
    }

    static String getPreferencesAsJSON(final Context context) {
        // Start the big JSON object
        final StringBuilder json = new StringBuilder("{");
        final Map<String, ?> prefsMap = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getAll();

        // Create JSON
        for (Map.Entry<String, ?> entry : prefsMap.entrySet()) {
            final String value = entry.getValue().toString();

            // Key is easy - always in quotes and has column afterwards
            json.append("\"").append(entry.getKey()).append("\":");

            // Value is a bit more complicated. Arrays, booleans and integers do not have quotes,
            // strings do. Finding array and boolean is easy, but finding integer needs trial and
            // error.
            if (value.startsWith("[") || value.equals("true") || value.equals("false")) {
                json.append(value);
            } else {
                try {
                    json.append(Integer.parseInt(value));
                } catch (NumberFormatException ignored) {
                    json.append("\"").append(value).append("\"");
                }
            }

            // Values always end with comma
            json.append(",");
        }

        // If we have keys, then we will have trailing comma - remove it
        if (prefsMap.size() > 0) {
            json.deleteCharAt(json.length() - 1);
        }

        // Finish JSON by closing the big object
        json.append('}');

        return json.toString();
    }

    static void copyToClipboard(View snackbarView, String title, String body) {
        final ClipboardManager clipboard = (ClipboardManager) snackbarView.getContext().getSystemService(CLIPBOARD_SERVICE);

        if (clipboard != null) {
            clipboard.setPrimaryClip(ClipData.newPlainText(title, body));

            Snackbar.make(snackbarView, R.string.copied_to_clipboard, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_error);

        expertModeActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> updatePreferencesAndRestart(result, getApplicationContext()));
    }

    public void showStacktrace(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        final AlertDialog dialog = new AlertDialog.Builder(CustomErrorActivity.this)
                .setTitle(R.string.error_details)
                .setMessage(CustomActivityOnCrash.getAllErrorDetailsFromIntent(CustomErrorActivity.this, getIntent()))
                .setPositiveButton(R.string.close, null)
                .setNeutralButton(R.string.copy_to_clipboard, (dialog_ignored, which_ignored) -> copyToClipboard(
                        findViewById(R.id.layout),
                        getString(R.string.error_details),
                        CustomActivityOnCrash.getAllErrorDetailsFromIntent(CustomErrorActivity.this, getIntent())
                ))
                .show();

        final TextView textView = dialog.findViewById(android.R.id.message);

        if (textView != null) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.customactivityoncrash_error_activity_error_details_text_size));
        }
    }

    public void restart(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        CustomActivityOnCrash.restartApplication(CustomErrorActivity.this, Objects.requireNonNull(CustomActivityOnCrash.getConfigFromIntent(getIntent())));
    }

    public void close(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        CustomActivityOnCrash.closeApplication(CustomErrorActivity.this, Objects.requireNonNull(CustomActivityOnCrash.getConfigFromIntent(getIntent())));
    }

    public void launchExpertMode(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        expertModeActivityResultLauncher.launch(new Intent(this, ExpertMode.class).putExtra("json", getPreferencesAsJSON(getApplicationContext())));
    }
}