package com.metal03326.ioio_otgheadunit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AutoStartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent arg1) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(arg1.getAction())) {
            context.startForegroundService(new Intent(context, IOIOHUService.class));
        }
    }
}
