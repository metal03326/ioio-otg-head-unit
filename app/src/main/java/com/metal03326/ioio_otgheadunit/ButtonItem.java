package com.metal03326.ioio_otgheadunit;

public class ButtonItem {
    private int min;
    private int max;
    private String name;
    private byte tapAction;
    private String tapActionSupplement;
    private byte holdAction;
    private String holdActionSupplement;

    public ButtonItem() {
        this.min = 0;
        this.max = 0;
        this.name = "";
        this.tapAction = 0;
        this.tapActionSupplement = "";
        this.holdAction = 0;
        this.holdActionSupplement = "";
    }

    public ButtonItem setMin(final int min) {
        this.min = min;

        return this;
    }

    public ButtonItem setMax(final int max) {
        this.max = max;

        return this;
    }

    public ButtonItem setName(final String name) {
        this.name = name;

        return this;
    }

    public ButtonItem setTapAction(final byte tabAction) {
        this.tapAction = tabAction;

        return this;
    }

    public ButtonItem setTapActionSupplement(final String tapActionSupplement) {
        this.tapActionSupplement = tapActionSupplement;

        return this;
    }

    public ButtonItem setHoldAction(final byte tabAction) {
        this.holdAction = tabAction;

        return this;
    }

    public ButtonItem setHoldActionSupplement(final String holdActionSupplement) {
        this.holdActionSupplement = holdActionSupplement;

        return this;
    }

    public int getMin() {
        return this.min;
    }

    public int getMax() {
        return this.max;
    }

    public String getName() {
        return this.name;
    }

    public byte getTapAction() {
        return this.tapAction;
    }

    public String getTapActionSupplement() {
        return this.tapActionSupplement;
    }

    public byte getHoldAction() {
        return this.holdAction;
    }

    public String getHoldActionSupplement() {
        return this.holdActionSupplement;
    }

    public ButtonItem[] addMeToArray(final ButtonItem[] buttonItems) {
        final int originalLength = buttonItems.length;
        final ButtonItem[] newButtonItems = new ButtonItem[originalLength + 1];

        System.arraycopy(buttonItems, 0, newButtonItems, 0, buttonItems.length);

        newButtonItems[originalLength] = this;

        return newButtonItems;
    }
}
