package com.metal03326.ioio_otgheadunit;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import java.util.Arrays;

import static com.metal03326.ioio_otgheadunit.IOIOHUService.LAUNCH_APP;
import static com.metal03326.ioio_otgheadunit.IOIOHUService.VOLUME_DOWN;
import static com.metal03326.ioio_otgheadunit.IOIOHUService.VOLUME_UP;

public class ButtonItemAdapter extends RecyclerView.Adapter<ButtonItemAdapter.ViewHolder> {
    private ButtonItem[] localDataSet;
    private ViewHolder[] viewHolders = {};
    final AppListAdapter appAdapter;
    final ArrayAdapter<CharSequence> buttonActionAdapter;
    final AdapterView.OnItemSelectedListener appDropdownListener;
    final AdapterView.OnItemSelectedListener buttonActionDropdownListener;

    private void setButtons(ButtonItem[] buttonItems) {
        LocalBroadcastManager.getInstance(appAdapter.getContext()).sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                        .putExtra("buttons", new Gson().toJson(buttonItems))
                        .putExtra("internalChange", true)
        );

        localDataSet = buttonItems;

        markItemsWithRangeErrors();
    }

    private void markItemsWithRangeErrors() {
        // Loop viewHolders, not data, as initially we do not have a viewHolder for every data set
        for (byte i = 0; i < viewHolders.length; i++) {
            final View fab = viewHolders[i].getInvalidResistanceRangesFab();
            final ButtonItem buttonItem = localDataSet[i];
            final int minValue = buttonItem.getMin();
            final int maxValue = buttonItem.getMax();

            // Force usage of Sort button - user has to sort items for error to disappear
            if (minValue <= 0 || maxValue <= 0 || minValue >= maxValue || (i + 1 < localDataSet.length && maxValue >= localDataSet[i + 1].getMin()) || (i > 0 && minValue <= localDataSet[i - 1].getMax())) {
                fab.setVisibility(View.VISIBLE);
            } else {
                fab.setVisibility(View.GONE);
            }
        }
    }

    private void addTextChangedListener(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // First part is the name of the field edited, second one is the index in the dataset
                final String[] tagData = String.valueOf(editText.getTag()).split("_");

                // Sanity check. If not 2, then it'll break
                if (tagData.length == 2) {
                    final String value = editText.getText().toString().trim();
                    final int index = Integer.parseInt(tagData[1]);
                    int intValue;
                    final ButtonItem[] buttonItems = new Gson().fromJson(appAdapter.getContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getString("buttons", "[]"), ButtonItem[].class);
                    final ButtonItem buttonItem = buttonItems[index];

                    try {
                        intValue = Integer.parseInt(value);
                    } catch (NumberFormatException ignored) {
                        intValue = 0;
                    }

                    switch (tagData[0]) {
                        case "name":
                            buttonItem.setName(value);
                            break;
                        case "min":
                            buttonItem.setMin(intValue);
                            break;
                        case "max":
                            buttonItem.setMax(intValue);
                            break;
                    }

                    setButtons(buttonItems);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final EditText name;
        private final MaterialButton delete;
        private final EditText min;
        private final EditText max;
        private final Spinner tapAction;
        private final Spinner tapActionSupplement;
        private final Spinner holdAction;
        private final Spinner holdActionSupplement;
        private final View invalidResistanceRanges;

        public ViewHolder(View view) {
            super(view);

            name = view.findViewWithTag("name");
            delete = view.findViewWithTag("delete");
            min = view.findViewWithTag("min");
            max = view.findViewWithTag("max");
            tapAction = view.findViewWithTag("tapAction");
            tapActionSupplement = view.findViewWithTag("tapActionSupplement");
            holdAction = view.findViewWithTag("holdAction");
            holdActionSupplement = view.findViewWithTag("holdActionSupplement");
            invalidResistanceRanges = view.findViewWithTag("invalidResistanceRanges");
        }

        public EditText getNameView() {
            return name;
        }

        public MaterialButton getDeleteButton() {
            return delete;
        }

        public EditText getMinView() {
            return min;
        }

        public EditText getMaxView() {
            return max;
        }

        public Spinner getTapActionSpinner() {
            return tapAction;
        }

        public Spinner getTapActionSupplementSpinner() {
            return tapActionSupplement;
        }

        public Spinner getHoldActionSpinner() {
            return holdAction;
        }

        public Spinner getHoldActionSupplementSpinner() {
            return holdActionSupplement;
        }

        public View getInvalidResistanceRangesFab() {
            return invalidResistanceRanges;
        }
    }

    public ButtonItemAdapter(final ButtonItem[] dataSet, final AppListAdapter adapter) {
        localDataSet = dataSet;
        appAdapter = adapter;

        buttonActionAdapter = ArrayAdapter.createFromResource(appAdapter.getContext(), R.array.button_actions, android.R.layout.simple_spinner_item);
        buttonActionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        appDropdownListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // First part is the name of the field edited, second one is the index in the dataset
                final String[] tagData = String.valueOf(adapterView.getTag()).split("_");

                // Sanity check. If not 2, then it'll break
                if (tagData.length == 2) {
                    final String applicationId = appAdapter.getItem(position).getId();
                    final ButtonItem[] buttonItems = new Gson().fromJson(appAdapter.getContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getString("buttons", "[]"), ButtonItem[].class);
                    final ButtonItem buttonItem = buttonItems[Integer.parseInt(tagData[1])];

                    switch (tagData[0]) {
                        case "tapActionSupplement":
                            buttonItem.setTapActionSupplement(applicationId);
                            break;
                        case "holdActionSupplement":
                            buttonItem.setHoldActionSupplement(applicationId);
                            break;
                    }

                    setButtons(buttonItems);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        };

        buttonActionDropdownListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // First part is the name of the field edited, second one is the index in the dataset
                final String[] tagData = String.valueOf(adapterView.getTag()).split("_");

                // Sanity check. If not 2, then it'll break
                if (tagData.length == 2) {
                    final byte index = Byte.parseByte(tagData[1]);
                    final ButtonItem[] buttonItems = new Gson().fromJson(appAdapter.getContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getString("buttons", "[]"), ButtonItem[].class);
                    final ButtonItem buttonItem = buttonItems[index];

                    switch (tagData[0]) {
                        case "tapAction":
                            buttonItem.setTapAction((byte) position);
                            break;
                        case "holdAction":
                            buttonItem.setHoldAction((byte) position);
                            break;
                    }

                    setButtons(buttonItems);

                    setupVisibilities(index);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.button_item, viewGroup, false));
    }

    private void setupVisibilities(final byte index) {
        final ButtonItem buttonItem = localDataSet[index];
        final ViewHolder currentViewHolder = viewHolders[index];
        final byte tapActionValue = buttonItem.getTapAction();
        final View tapActionSupplementMarginContainer = (View) currentViewHolder.getTapActionSupplementSpinner().getParent();
        final View holdActionMarginContainer = (View) currentViewHolder.getHoldActionSpinner().getParent();
        final View holdActionSupplementMarginContainer = (View) currentViewHolder.getHoldActionSupplementSpinner().getParent();
        int tapActionSupplementVisibility = View.VISIBLE;
        int holdActionVisibility = View.VISIBLE;
        int holdActionSupplementVisibility = View.VISIBLE;

        if (buttonItem.getHoldAction() != LAUNCH_APP) {
            holdActionSupplementVisibility = View.GONE;
        }

        if (tapActionValue == VOLUME_UP || tapActionValue == VOLUME_DOWN) {
            tapActionSupplementVisibility = View.GONE;
            holdActionVisibility = View.GONE;
            holdActionSupplementVisibility = View.GONE;
        } else if (tapActionValue != LAUNCH_APP) {
            tapActionSupplementVisibility = View.GONE;
            holdActionVisibility = View.VISIBLE;
        }

        ((ViewGroup.MarginLayoutParams) ((View) currentViewHolder.getTapActionSpinner().getParent()).getLayoutParams()).setMarginEnd(tapActionSupplementVisibility == View.GONE ? 0 : ((ViewGroup.MarginLayoutParams) tapActionSupplementMarginContainer.getLayoutParams()).getMarginStart());
        ((ViewGroup.MarginLayoutParams) holdActionMarginContainer.getLayoutParams()).setMarginEnd(holdActionSupplementVisibility == View.GONE ? 0 : ((ViewGroup.MarginLayoutParams) holdActionSupplementMarginContainer.getLayoutParams()).getMarginStart());

        ((View) tapActionSupplementMarginContainer.getParent()).setVisibility(tapActionSupplementVisibility);
        ((View) holdActionMarginContainer.getParent()).setVisibility(holdActionVisibility);
        ((View) holdActionSupplementMarginContainer.getParent()).setVisibility(holdActionSupplementVisibility);
    }

    final public void setupTextField(final EditText field, final String value, final String tagSuffix) {
        final String[] tagData = String.valueOf(field.getTag()).split("_");

        // Setup only first time
        if (tagData.length == 1) {
            // Set value before adding listener
            field.setText(value);

            // Setup tag to include index so listener can work properly
            field.setTag(field.getTag() + tagSuffix);

            // Attach listener
            addTextChangedListener(field);

            markItemsWithRangeErrors();
        }
    }

    final public void setupActionButtonSpinner(final Spinner spinner, final byte value, final String tagSuffix) {
        final String[] tagData = String.valueOf(spinner.getTag()).split("_");

        // Setup only first time
        if (tagData.length == 1) {
            spinner.setAdapter(buttonActionAdapter);
            // Make sure we cannot get out of bounds
            spinner.setSelection(Math.max(0, Math.min(value, buttonActionAdapter.getCount() - 1)));
            spinner.setTag(spinner.getTag() + tagSuffix);

            // Unlike EditText, adding the listener after setting of the selection doesn't prevent initial firing of the listener, so we delay
            new Handler().postDelayed(() -> spinner.setOnItemSelectedListener(buttonActionDropdownListener), 300);
        }
    }

    final public void setupActionButtonSupplementSpinner(final Spinner spinner, final String value, final String tagSuffix) {
        final String[] tagData = String.valueOf(spinner.getTag()).split("_");

        // Setup only first time
        if (tagData.length == 1) {
            spinner.setAdapter(appAdapter);

            for (int i = 0; i < appAdapter.getCount(); i++) {
                AppListItem appListItem = appAdapter.getItem(i);

                if (value.equals(appListItem.getId())) {
                    spinner.setSelection(i);
                    break;
                }
            }
            spinner.setTag(spinner.getTag() + tagSuffix);

            // Unlike EditText, adding the listener after setting of the selection doesn't prevent initial firing of the listener, so we delay
            new Handler().postDelayed(() -> spinner.setOnItemSelectedListener(appDropdownListener), 300);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int index) {
        final ButtonItem buttonItem = localDataSet[index];
        // All views will get the index in the dataset in their tag - used in change listeners to know which datum to change
        final String tagSuffix = "_" + index;

        // Save reference to the viewHolder so setupVisibilities can show/hide views inside of is
        viewHolders = Arrays.copyOf(viewHolders, viewHolders.length + 1);
        viewHolders[viewHolders.length - 1] = viewHolder;

        setupTextField(viewHolder.getNameView(), buttonItem.getName(), tagSuffix);
        setupTextField(viewHolder.getMinView(), String.valueOf(buttonItem.getMin()), tagSuffix);
        setupTextField(viewHolder.getMaxView(), String.valueOf(buttonItem.getMax()), tagSuffix);

        // Set index for the listener to know which item to delete
        viewHolder.getDeleteButton().setTag((byte) index);

        setupActionButtonSpinner(viewHolder.getTapActionSpinner(), buttonItem.getTapAction(), tagSuffix);
        setupActionButtonSpinner(viewHolder.getHoldActionSpinner(), buttonItem.getHoldAction(), tagSuffix);

        setupVisibilities((byte) index);

        setupActionButtonSupplementSpinner(viewHolder.getTapActionSupplementSpinner(), buttonItem.getTapActionSupplement(), tagSuffix);
        setupActionButtonSupplementSpinner(viewHolder.getHoldActionSupplementSpinner(), buttonItem.getHoldActionSupplement(), tagSuffix);
    }

    @Override
    public int getItemCount() {
        return localDataSet.length;
    }
}
