package com.metal03326.ioio_otgheadunit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class NoOp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Do not do anything. Foreground service will decide should it wakeup the screen or not
        finish();
    }
}