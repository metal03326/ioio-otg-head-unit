package com.metal03326.ioio_otgheadunit;

import static com.metal03326.ioio_otgheadunit.SettingsActivity.NOTIFY_NONE;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

public class StartupSettings extends Fragment {
    View layout;
    SharedPreferences preferences;
    private ActivityResultLauncher<Intent> galleryActivityResultLauncher;
    private ActivityResultLauncher<String> storagePermissionActivityResultLauncher;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        galleryActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == AppCompatActivity.RESULT_OK && result.getData() != null && result.getData().getData() != null) {
                        final String splashImageStr = ImageFilePath.getPath(requireActivity().getApplicationContext(), result.getData().getData());

                        if (splashImageStr == null) {
                            Snackbar.make(layout, R.string.try_internal_storage_path, Snackbar.LENGTH_LONG).show();
                        } else {
                            setSplashImage(splashImageStr, true);
                        }
                    }
                });

        storagePermissionActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if (isGranted) {
                openGalleryForSelection();
            } else {
                Snackbar.make(layout, R.string.no_photos_permission_error, Snackbar.LENGTH_LONG).show();
            }
        });

        return inflater.inflate(R.layout.fragment_startup_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layout = getView();
        preferences = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);

        RecyclerView startupSwitchesRecyclerView = layout.findViewWithTag("startupSwitches");

        String[][] startupSwitches = {
                {"wakeupScreen", getString(R.string.wakeup_screen_on_connect), getString(R.string.wakeup_screen_on_connect_help)},
                {"autoPlay", getString(R.string.resume_playback_on_connect), getString(R.string.resume_playback_on_connect_help)},
                {"autoUnmute", getString(R.string.unmute_on_connect), getString(R.string.unmute_on_connect_help)},
                {"actionsDelay", getString(R.string.actions_delay), getString(R.string.actions_delay_help)},
        };

        startupSwitchesRecyclerView.setHasFixedSize(true);
        startupSwitchesRecyclerView.setAdapter(new SwitchItemAdapter(startupSwitches, preferences, () -> ((SettingsActivity) requireActivity()).setFabsVisibilities(NOTIFY_NONE, new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class))));

        AppListAdapter adapter = ((SettingsActivity) requireActivity()).getAppListAdapter();
        Spinner appOnConnect = layout.findViewWithTag("appOnConnect");
        String packageId = preferences.getString("appOnConnect", "");

        appOnConnect.setAdapter(adapter);

        if (!packageId.equals("")) {
            for (int i = 0; i < adapter.getCount(); i++) {
                AppListItem appListItem = adapter.getItem(i);

                if (packageId.equals(appListItem.getId())) {
                    appOnConnect.setSelection(i);
                    break;
                }
            }
        }

        // Unlike EditText, adding the listener after setting of the selection doesn't prevent initial firing of the listener, so we delay
        new Handler().postDelayed(() -> appOnConnect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                preferences.edit().putString("appOnConnect", adapter.getItem(position).getId()).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        }), 300);

        final EditText splashVisibleForText = layout.findViewWithTag("splashVisibleFor");
        final String splashImageStr = preferences.getString("splashImage", "");
        byte splashVisibleFor = 0;

        if (!splashImageStr.equals("")) {
            setSplashImage(splashImageStr, false);
        }

        // In case user did something wrong with integer value in Expert mode
        try {
            splashVisibleFor = (byte) preferences.getInt("splashVisibleFor", 0);
        } catch (Exception ignored) {
        }

        if (splashVisibleFor != 0) {
            splashVisibleForText.setText(String.valueOf(splashVisibleFor));
        }

        splashVisibleForText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                byte value;

                try {
                    value = Byte.parseByte(splashVisibleForText.getText().toString());
                } catch (Exception ignored) {
                    value = 0;
                }

                preferences.edit().putInt("splashVisibleFor", value).apply();

                validateSplashScreenSettings();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        validateSplashScreenSettings();

        layout.findViewWithTag("openGallery").setOnClickListener(this::openGallery);
        layout.findViewWithTag("removeSplashImage").setOnClickListener(this::removeSplashImage);
        layout.findViewWithTag("startSplashScreen").setOnClickListener(this::startSplashScreen);
    }

    private void validateSplashScreenSettings() {
        final boolean noSplashImage = preferences.getString("splashImage", "").equals("");
        boolean noValidSettings = true;

        // In case user did something wrong with integer value in Expert mode
        try {
            noValidSettings = noSplashImage || preferences.getInt("splashVisibleFor", 0) == 0;
        } catch (Exception ignored) {
        }

        layout.findViewWithTag("removeSplashImage").setVisibility(noSplashImage ? View.GONE : View.VISIBLE);
        layout.findViewWithTag("startSplashScreen").setVisibility(noValidSettings ? View.GONE : View.VISIBLE);
        layout.findViewWithTag("invalidSplashScreenSettings").setVisibility(noValidSettings ? View.VISIBLE : View.GONE);
    }

    public void setSplashImage(final String splashImageStr, final boolean saveAndValidate) {
        final pl.droidsonroids.gif.GifImageView splashImage = layout.findViewWithTag("splashImage");

        if (splashImageStr.equals("")) {
            splashImage.setImageResource(R.drawable.app_icon_foreground);
        } else {
            if (Build.VERSION.SDK_INT >= 29) {
                try (ParcelFileDescriptor pfd = requireActivity().getContentResolver().openFileDescriptor(Uri.fromFile(new File(splashImageStr)), "r")) {
                    if (pfd != null) {
                        splashImage.setImageBitmap(BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor()));
                    }
                } catch (IOException ignored) {
                }
            } else {
                splashImage.setImageBitmap(BitmapFactory.decodeFile(splashImageStr));
            }
        }

        if (saveAndValidate) {
            preferences.edit().putString("splashImage", splashImageStr).apply();

            validateSplashScreenSettings();
        }
    }

    private void openGalleryForSelection() {
        galleryActivityResultLauncher.launch(Intent.createChooser(
                new Intent()
                        .setType("image/*")
                        .setAction(Intent.ACTION_OPEN_DOCUMENT)
                        .addCategory(Intent.CATEGORY_OPENABLE)
                , getString(R.string.select_picture))
        );
    }

    public void startSplashScreen(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        requireActivity().getApplicationContext().startActivity(
                new Intent(requireActivity().getApplicationContext(), SplashActivity.class)
                        .setAction(Intent.ACTION_VIEW)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("test", true)
        );
    }

    public void removeSplashImage(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        setSplashImage("", true);
    }

    public void openGallery(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        if (ActivityCompat.checkSelfPermission(requireActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            storagePermissionActivityResultLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
        } else {
            openGalleryForSelection();
        }
    }
}