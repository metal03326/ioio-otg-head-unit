package com.metal03326.ioio_otgheadunit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;

public class ExpertMode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_mode);

        try {
            // Pretty print JSON
            ((EditText) findViewById(R.id.json)).setText(new JSONObject(getIntent().getExtras().getString("json", "{}")).toString(2));
        } catch (Exception ignored) {
        }
    }

    public void saveAndRestart(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        setResult(Activity.RESULT_OK, new Intent().putExtra("result", ((EditText) findViewById(R.id.json)).getText().toString()));

        finish();
    }

    public void copyErrorToClipboard(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        CustomErrorActivity.copyToClipboard(findViewById(R.id.layout), getString(R.string.preferences_as_json), ((EditText) findViewById(R.id.json)).getText().toString());
    }
}