package com.metal03326.ioio_otgheadunit;

import com.google.android.material.snackbar.Snackbar;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager2.widget.ViewPager2;

import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.content.ComponentName;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import cat.ereza.customactivityoncrash.config.CaocConfig;

public class SettingsActivity extends AppCompatActivity {
    private ActivityResultLauncher<Intent> deviceAdminActivityResultLauncher;
    private ActivityResultLauncher<Intent> accessibilityActivityResultLauncher;
    static final byte NOTIFY_NONE = 0;
    static final byte NOTIFY_ACCESSIBILITY = 1;
    static final byte NOTIFY_ADMIN = 2;

    ViewGroup layout;
    SharedPreferences preferences;
    AppListAdapter adapter;
    ViewPager2 viewPager;
    LocalBroadcastManager lbm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Attach custom crash screen
        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT)
                .trackActivities(true)
                .errorActivity(CustomErrorActivity.class)
                .apply();

        setContentView(R.layout.activity_settings);

        deviceAdminActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> setFabsVisibilities(NOTIFY_ADMIN, new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class)));

        accessibilityActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> setFabsVisibilities(NOTIFY_ACCESSIBILITY, new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class)));

        layout = findViewById(R.id.layout);
        lbm = LocalBroadcastManager.getInstance(this);
        preferences = getApplicationContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);

        int ver = 0;
        // In case user did something wrong with integer value in Expert mode
        try {
            ver = preferences.getInt("ver", 0);
        } catch (Exception ignored) {
        }

        // First things first - migrate data
        migratePreferences(ver);

        createTabs();

        setFabsVisibilities(NOTIFY_NONE, new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class));

        // After everything is setup, start the foreground service.
        // This used to be at the top, but new custom Error screen was not ready in time if error
        // occurs inside of the service.
        startForegroundService(new Intent(this, IOIOHUService.class));

        // Ask service to update us when it started
        lbm.sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                        .putExtra("getStartedAt", true)
        );
    }

    private void createTabs() {
        viewPager = layout.findViewWithTag("viewPager");
        TabLayout tabLayout = layout.findViewWithTag("tabLayout");

        int[] tabIcons = {
                R.drawable.ic_baseline_settings_24,
                R.drawable.ic_baseline_admin_panel_settings_24,
                R.drawable.ic_baseline_power_settings_new_24,
                R.drawable.ic_baseline_usb_24,
                R.drawable.ic_baseline_pie_chart_outline_24,
                R.drawable.ic_baseline_bug_report_48
        };

        // Create the tabs
        for (int i = 0; i < tabIcons.length; i++) {
            tabLayout.addTab(tabLayout.newTab());
        }

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(new TabPagerAdapter(this, tabLayout.getTabCount()));

        // Disable smooth scroll as that instantiates all fragments up to the clicked one, which
        // takes different time to happen and order gets messy
        new TabLayoutMediator(tabLayout, viewPager, true, false, (tab, position) -> tab.setIcon(tabIcons[position])).attach();
    }

    public void onDeleteButtonClick(final View view) {
        ((SteeringWheelButtonsFragment) Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag("f" + viewPager.getCurrentItem()))).onDeleteButtonClick((byte) view.getTag());
    }

    public AppListAdapter getAppListAdapter() {
        if (adapter == null) {
            // Get list of installed applications
            final List<ApplicationInfo> apps = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
            // List of dropdown items will start with No App item, so length is 1 more than the
            // list of installed applications
            final AppListItem[] appItems = new AppListItem[apps.size() + 1];

            // Create the No App item with name as an empty string, so sorting will leave it at
            // first position
            appItems[0] = new AppListItem();

            // Fill the rest of the dropdown items
            for (int i = 0; i < apps.size(); i++) {
                final ApplicationInfo applicationInfo = apps.get(i);
                final int index = i + 1;
                final AppListItem appItem = new AppListItem();

                appItem.setId(applicationInfo.packageName);
                appItem.setName(String.valueOf(applicationInfo.loadLabel(getPackageManager())));

                try {
                    appItem.setIcon(getPackageManager().getApplicationIcon(applicationInfo.packageName));
                } catch (Exception ignored) {
                }

                appItems[index] = appItem;
            }

            // Sort by humanly readable name, a-Z
            Arrays.sort(appItems, (i1, i2) -> i1.getName().compareToIgnoreCase(i2.getName()));

            // After sorting set No App item #1
            appItems[0].setName(getString(R.string.no_app));

            // Set our app icon for the No App
            try {
                appItems[0].setIcon(getPackageManager().getApplicationIcon(BuildConfig.APPLICATION_ID));
            } catch (PackageManager.NameNotFoundException ignored) {
            }

            // Save the adapter for later use
            adapter = new AppListAdapter(SettingsActivity.this, android.R.layout.simple_list_item_1, appItems);
        }

        return adapter;
    }

    private void migratePreferences(final int ver) {
        // This is ver for next recursive iteration. It can be changed in this iteration, if data is
        // migrated
        int latestVer = ver;

        switch (ver) {
            // First migration
            case 0:
                String buttons = preferences.getString("buttons", "[]");

                // Change is in buttons, so do not do anything if there are no buttons. This also
                // covers the newly installed app case, where app doesn't actually require
                // migration, but it also doesn't have ver set
                if (!buttons.equals("[]")) {
                    final Gson gson = new Gson();
                    final ButtonItem[] buttonItems = gson.fromJson(buttons, ButtonItem[].class);

                    // Fast forward and Rewind got added after item 6
                    for (ButtonItem buttonItem : buttonItems) {
                        final byte tapAction = buttonItem.getTapAction();
                        final byte holdAction = buttonItem.getHoldAction();

                        if (tapAction > 6) {
                            buttonItem.setTapAction((byte) (tapAction + 2));
                        }

                        if (holdAction > 6) {
                            buttonItem.setHoldAction((byte) (holdAction + 2));
                        }
                    }

                    buttons = gson.toJson(buttonItems);

                    // Move version to 6 and save changes
                    preferences.edit()
                            .putString("buttons", buttons)
                            .apply();

                    // Notify service, in case it is running. This will save again, but we need to make
                    // sure data is proper there as well
                    lbm.sendBroadcast(
                            new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                                    .putExtra("buttons", buttons)
                    );
                }

                // Set ver to 6 (versionCode as of writing this comment)
                latestVer = 6;
                break;
            default:
                // We reached the end of the recursion, save the last migration version
                preferences.edit().putInt("ver", ver).apply();
        }

        // Migration works recursively. Each run executes only one migration and moves ver to point
        // to that migration version. In theory ver can be any number and can even be smaller number
        // than previous ver, but to make things easier to understand, ver actually represents the
        // versionCode at which migration was necessary.
        if (latestVer != ver) {
            // Run migration again, in case there are more changes to be applied
            migratePreferences(latestVer);
        }
    }

    private boolean isAccessibilitySettingsOn(final Context mContext) {
        int accessibilityEnabled = 0;

        try {
            accessibilityEnabled = Settings.Secure.getInt(mContext.getApplicationContext().getContentResolver(), android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException ignored) {
        }

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(mContext.getApplicationContext().getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);

            if (settingValue != null) {
                final TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

                mStringColonSplitter.setString(settingValue);

                while (mStringColonSplitter.hasNext()) {
                    if (mStringColonSplitter.next().equalsIgnoreCase(getPackageName() + "/" + MyAccessibilityService.class.getCanonicalName())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public void warningShow(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        int title = 0;
        int message = 0;
        String messageStr = null;
        final AlertDialog.Builder alert = new AlertDialog.Builder(this).setPositiveButton(R.string.close, null);

        switch (view.getTag().toString()) {
            case "invalidSplashScreenSettings":
                title = R.string.no_splash_screen_will_be_shown;
                message = R.string.no_splash_screen_will_be_shown_explanation;
                break;
            case "invalidKnowResistor":
                title = R.string.invalid_known_resistor;
                message = R.string.invalid_known_resistor_explanation;
                break;
            case "invalidResistanceRanges":
                title = R.string.invalid_range;
                message = R.string.invalid_range_explanation;
                break;
            case "noWriteSecureSettingsPermission":
                title = R.string.write_secure_settings_required;
                String command = getString(R.string.write_secure_settings_command);
                messageStr = getString(R.string.write_secure_settings_explanation, command);

                alert.setNeutralButton(R.string.copy_command_to_clipboard, (dialog_ignored, which_ignored) -> CustomErrorActivity.copyToClipboard(layout, getString(R.string.adb_command), command));
                break;
        }

        alert
                .setTitle(getString(title))
                .setMessage(Html.fromHtml(messageStr != null ? messageStr : getString(message), Html.FROM_HTML_MODE_LEGACY))
                .show();
    }

    public void setFabsVisibilities(final byte notifyType, final ButtonItem[] buttonItems) {
        // Set visibility of the device admin fab
        final int deviceAdminVisibility = ((DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE)).isAdminActive(new ComponentName(this, MyDeviceAdminReceiver.class)) ? View.GONE : View.VISIBLE;
        final boolean isAccessibilityOn = isAccessibilitySettingsOn(getApplicationContext());
        int accessibilityVisibility;

        if (isAccessibilityOn) {
            accessibilityVisibility = View.GONE;
        } else {
            boolean hasGlobalActions = false;

            // In case user did something wrong with boolean value in Expert mode
            try {
                hasGlobalActions = preferences.getBoolean("briefShowNotificationsTrackChange", false);
            } catch (Exception ignored) {
            }

            if (!hasGlobalActions) {
                for (ButtonItem buttonItem : buttonItems) {
                    if (IOIOHUService.GLOBAL_ACTIONS.contains(buttonItem.getTapAction()) || IOIOHUService.GLOBAL_ACTIONS.contains(buttonItem.getHoldAction())) {
                        hasGlobalActions = true;
                        break;
                    }
                }
            }

            accessibilityVisibility = hasGlobalActions ? View.VISIBLE : View.GONE;
        }

        // Set proper visibilities and padding
        layout.findViewWithTag("deviceAdminFab").setVisibility(deviceAdminVisibility);
        layout.findViewWithTag("accessibilityServiceFab").setVisibility(accessibilityVisibility);
        layout.findViewWithTag("viewPager").setPaddingRelative(16, 8, 16, deviceAdminVisibility == View.VISIBLE || accessibilityVisibility == View.VISIBLE ? (int) (72 * getResources().getDisplayMetrics().density) : 0);

        // If we need to show result to the user, decide text based on the visibility of the fab
        if (notifyType == NOTIFY_ADMIN) {
            Snackbar.make(layout, deviceAdminVisibility == View.GONE ? R.string.device_admin_rights_granted : R.string.device_admin_rights_not_granted, Snackbar.LENGTH_LONG).show();
        } else if (notifyType == NOTIFY_ACCESSIBILITY) {
            Snackbar.make(layout, isAccessibilityOn ? R.string.accessibility_service_activated : R.string.accessibility_service_not_activated, Snackbar.LENGTH_LONG).show();
        }
    }

    public void enableDeviceAdmin(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.device_admin_rights_required)
                .setMessage(R.string.device_admin_rights_explanation)
                .setPositiveButton(R.string.go_to_settings, (dialog, whichButton) -> deviceAdminActivityResultLauncher.launch(
                        new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
                                .putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, new ComponentName(this, MyDeviceAdminReceiver.class))
                ))
                .setNegativeButton(android.R.string.cancel, null).show();
    }

    public void enableAccessibilityService(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.accessibility_service_not_enabled)
                .setMessage(R.string.accessibility_service_not_enabled_explanation)
                .setPositiveButton(R.string.go_to_settings, (dialog, whichButton) -> accessibilityActivityResultLauncher.launch(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)))
                .setNegativeButton(android.R.string.cancel, null).show();
    }
}
