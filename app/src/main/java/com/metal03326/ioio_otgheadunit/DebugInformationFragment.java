package com.metal03326.ioio_otgheadunit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DebugInformationFragment extends Fragment {
    View layout;
    SharedPreferences preferences;
    LocalBroadcastManager lbm;
    private ActivityResultLauncher<Intent> expertModeActivityResultLauncher;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        expertModeActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> CustomErrorActivity.updatePreferencesAndRestart(result, requireActivity().getApplicationContext()));

        return inflater.inflate(R.layout.fragment_debug_information, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layout = getView();
        preferences = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        lbm = LocalBroadcastManager.getInstance(requireActivity());

        showDebugValue(getString(R.string.app_version, BuildConfig.VERSION_NAME), "appVer");
        showDebugValue(getString(R.string.measured_resistance_with_column, "").replace("\u2126", ""), "resistance");
        showDebugValue(getString(R.string.last_active_button, ""), "activeButton");

        layout.findViewWithTag("onTapClick").setOnClickListener(this::onTapClick);
        layout.findViewWithTag("onHoldClick").setOnClickListener(this::onHoldClick);
        layout.findViewWithTag("debugHardReset").setOnClickListener(this::debugHardReset);
        layout.findViewWithTag("launchExpertMode").setOnClickListener(this::launchExpertMode);
    }

    @Override
    public void onPause() {
        super.onPause();

        lbm.unregisterReceiver(resistance_report_receiver);
    }

    @Override
    public void onResume() {
        super.onResume();

        final List<String> spinnerArray = new ArrayList<>();

        for (ButtonItem buttonItem : new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class)) {
            spinnerArray.add(buttonItem.getName());
        }

        final ArrayAdapter<String> debugAdapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_item, spinnerArray);

        debugAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ((Spinner) layout.findViewWithTag("debugExecuteButton")).setAdapter(debugAdapter);

        lbm.registerReceiver(resistance_report_receiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".resistance_report"));
    }

    public void onTapClick(final View view) {
        debugExecuteButton("tap");
    }

    public void onHoldClick(final View view) {
        debugExecuteButton("hold");
    }

    public void debugExecuteButton(final String type) {
        lbm.sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                        .putExtra("emulateButton", (byte) ((Spinner) layout.findViewWithTag("debugExecuteButton")).getSelectedItemPosition())
                        .putExtra("type", type)
        );
    }

    public void debugHardReset(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        lbm.sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                        .putExtra("hardReset", true)
        );
    }

    public void launchExpertMode(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        expertModeActivityResultLauncher.launch(new Intent(requireActivity(), ExpertMode.class).putExtra("json", CustomErrorActivity.getPreferencesAsJSON(requireActivity().getApplicationContext())));
    }

    private void showDebugValue(final String val, final String tag) {
        requireActivity().runOnUiThread(() -> ((TextView) layout.findViewWithTag(tag)).setText(val));
    }

    public final BroadcastReceiver resistance_report_receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                showDebugValue(intent.getStringExtra("value"), intent.getStringExtra("tag"));
            }
        }
    };
}