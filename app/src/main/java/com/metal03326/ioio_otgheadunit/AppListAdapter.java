package com.metal03326.ioio_otgheadunit;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class AppListAdapter extends ArrayAdapter<AppListItem> {
    private final AppListItem[] values;

    public AppListAdapter(final Context context, final int textViewResourceId, final AppListItem[] appListItems) {
        super(context, textViewResourceId, appListItems);

        values = appListItems;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public AppListItem getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return this.getDropDownView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        final TextView label = (TextView) super.getView(position, convertView, parent);
        final Drawable icon = values[position].getIcon();

        label.setTextColor(Color.BLACK);
        label.setText(values[position].getName());
        label.setCompoundDrawablePadding(20);
        label.setSingleLine(true);
        label.setEllipsize(TextUtils.TruncateAt.END);

        if (icon != null) {
            icon.setBounds(0, 0, 54, 54);
            label.setCompoundDrawables(icon, null, null, null);
        }

        return label;
    }
}
