package com.metal03326.ioio_otgheadunit;

import static com.metal03326.ioio_otgheadunit.SettingsActivity.NOTIFY_NONE;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.Arrays;

public class SteeringWheelButtonsFragment extends Fragment {
    View layout;
    SharedPreferences preferences;
    LocalBroadcastManager lbm;
    //todo: What if low device memory? SettingsActivity will die and will not restart with
    // SteeringWheelButtonsFragment rendered, so we are not going to catch the result. Either we
    // need to move this back to SettingsActivity, or we need to start opening last used tab at
    // launch. The first makes SettingsActivity dirty, while the second makes app slow if user left
    // it on a heavy tab.
    private ActivityResultLauncher<Intent> autoDetectActivityResultLauncher;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        autoDetectActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    final Intent intent = new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test");

                    // User exited Auto Detect - enable service to start executing commands on
                    // button press
                    intent.putExtra("enable", true);

                    if (result.getResultCode() == AppCompatActivity.RESULT_OK && result.getData() != null) {
                        final Gson gson = new Gson();
                        final ButtonItem[] resultingButtonItems = gson.fromJson(result.getData().getStringExtra("result"), ButtonItem[].class);
                        final ButtonItem[] buttonItems = gson.fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class);

                        // Compare old button names with new ones and if there is a case sensitive
                        // match, copy action settings
                        for (ButtonItem resultingButtonItem : resultingButtonItems) {
                            for (ButtonItem buttonItem : buttonItems) {
                                if (buttonItem.getName().equals(resultingButtonItem.getName())) {
                                    resultingButtonItem
                                            .setHoldAction(buttonItem.getHoldAction())
                                            .setHoldActionSupplement(buttonItem.getHoldActionSupplement())
                                            .setTapAction(buttonItem.getTapAction())
                                            .setTapActionSupplement(buttonItem.getTapActionSupplement());

                                    break;
                                }
                            }
                        }

                        // Send buttons to the service, where they will be saved as well
                        intent.putExtra("buttons", gson.toJson(resultingButtonItems));
                    }

                    lbm.sendBroadcast(intent);
                });

        return inflater.inflate(R.layout.fragment_steering_wheel_buttons, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layout = getView();
        preferences = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        lbm = LocalBroadcastManager.getInstance(requireActivity());

        // Listen for when buttons are changed and run validation on the new buttons
        lbm.registerReceiver(buttons_receiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".service_setup_and_test"));

        updateButtonsViews(new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class), false);

        layout.findViewWithTag("onAutoDetectClick").setOnClickListener(this::onAutoDetectClick);
        layout.findViewWithTag("onAddButtonClick").setOnClickListener(this::onAddButtonClick);
        layout.findViewWithTag("onSortClick").setOnClickListener(this::onSortClick);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        lbm.unregisterReceiver(buttons_receiver);
    }

    public void updateButtonsViews(final ButtonItem[] buttonItems, boolean internalChange) {
        // Show/Hide cards for accessibility service, but do not show snackbar
        ((SettingsActivity) requireActivity()).setFabsVisibilities(NOTIFY_NONE, buttonItems);

        // Re-create views with new adapter, as we do not know what changed
        if (!internalChange) {
            ((RecyclerView) layout.findViewWithTag("dynamicButtons")).setAdapter(new ButtonItemAdapter(buttonItems, ((SettingsActivity) requireActivity()).getAppListAdapter()));
        }
    }

    public void onAutoDetectClick(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.auto_detect_warning_title)
                .setMessage(R.string.auto_detect_warning_explanation)
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                    // Make foreground service not to execute button actions when user presses a button
                    lbm.sendBroadcast(
                            new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                                    .putExtra("enable", false)
                    );

                    autoDetectActivityResultLauncher.launch(new Intent(requireActivity(), AutoDetectActivity.class));
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void onAddButtonClick(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        final Gson gson = new Gson();
        ButtonItem[] buttonItems = gson.fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class);

        lbm.sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                        .putExtra("buttons", gson.toJson(
                                new ButtonItem()
                                        .setName(String.valueOf(buttonItems.length))
                                        .addMeToArray(buttonItems)
                        ))
        );
    }

    public void onDeleteButtonClick(byte index) {
        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.are_you_sure)
                .setMessage(R.string.delete_button_explanation)
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                    final Gson gson = new Gson();
                    final ButtonItem[] buttonItems = gson.fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class);
                    final ButtonItem[] newButtonItems = new ButtonItem[buttonItems.length - 1];

                    // Remove button
                    System.arraycopy(buttonItems, 0, newButtonItems, 0, index);
                    System.arraycopy(buttonItems, index + 1, newButtonItems, index, buttonItems.length - index - 1);

                    lbm.sendBroadcast(
                            new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                                    .putExtra("buttons", gson.toJson(newButtonItems))
                    );
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void onSortClick(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        final Gson gson = new Gson();
        final ButtonItem[] buttonItems = gson.fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class);

        Arrays.sort(buttonItems, (b1, b2) -> b1.getMin() - b2.getMin());

        lbm.sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                        .putExtra("buttons", gson.toJson(buttonItems))
        );
    }

    public final BroadcastReceiver buttons_receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final String buttons = intent.getStringExtra("buttons");

                // Many things can be passed in this receiver, we are interested only in "buttons"
                if (buttons != null) {
                    // Update views and run validation with the new buttons
                    updateButtonsViews(new Gson().fromJson(buttons, ButtonItem[].class), intent.getBooleanExtra("internalChange", false));
                }
            }
        }
    };
}