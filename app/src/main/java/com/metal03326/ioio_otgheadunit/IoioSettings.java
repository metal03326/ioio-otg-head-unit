package com.metal03326.ioio_otgheadunit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class IoioSettings extends Fragment {
    View layout;
    SharedPreferences preferences;
    LocalBroadcastManager lbm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ioio_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layout = getView();
        preferences = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        lbm = LocalBroadcastManager.getInstance(requireActivity());

        final EditText knownResistor = layout.findViewWithTag("knownResistor");
        final Spinner ioioPin = layout.findViewWithTag("ioioPin");

        // First set listener, so when we load value from preferences, this will fire and validation
        // will happen, showing/hiding error message
        knownResistor.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                final View invalidKnowResistor = layout.findViewWithTag("invalidKnowResistor");
                int R1;

                try {
                    R1 = Integer.parseInt(knownResistor.getText().toString());

                    invalidKnowResistor.setVisibility(R1 == 0 ? View.VISIBLE : View.GONE);
                } catch (Exception ignored) {
                    R1 = 0;

                    invalidKnowResistor.setVisibility(View.VISIBLE);
                }

                lbm.sendBroadcast(
                        new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                                .putExtra("knownResistor", R1)
                );

                preferences.edit().putInt("knownResistor", R1).apply();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        // In case user did something wrong with integer value in Expert mode
        try {
            // Resistance cannot be negative
            knownResistor.setText(String.valueOf(Math.max(0, preferences.getInt("knownResistor", 0))));
            // Make sure we cannot get out of bounds
            ioioPin.setSelection(Math.max(0, Math.min(preferences.getInt("ioioPin", 0), ioioPin.getAdapter().getCount() - 1)));
        } catch (Exception ignored) {
        }

        ioioPin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                lbm.sendBroadcast(
                        new Intent(BuildConfig.APPLICATION_ID + ".service_setup_and_test")
                                .putExtra("ioioPin", (byte) position)
                );

                preferences.edit().putInt("ioioPin", position).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }
}