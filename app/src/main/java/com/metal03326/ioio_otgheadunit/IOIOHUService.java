package com.metal03326.ioio_otgheadunit;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.KeyEvent;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.IOIO;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOService;

import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_BACK;
import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_HOME;
import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_NOTIFICATIONS;
import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_RECENTS;
import static androidx.core.app.NotificationCompat.PRIORITY_MAX;

/**
 * An example IOIO service. While this service is alive, it will attempt to connect to a IOIO and
 * blink the LED. A notification will appear on the notification bar, enabling the user to stop the
 * service.
 */
public class IOIOHUService extends IOIOService {
    public static final byte NO_ACTION = 0;
    public static final byte PLAY = 1;
    public static final byte PLAY_PAUSE = 2;
    public static final byte PAUSE = 3;
    public static final byte STOP = 4;
    public static final byte NEXT = 5;
    public static final byte PREVIOUS = 6;
    public static final byte FORWARD = 7;
    public static final byte REWIND = 8;
    public static final byte VOLUME_UP = 9;
    public static final byte VOLUME_DOWN = 10;
    public static final byte MUTE = 11;
    public static final byte UNMUTE = 12;
    public static final byte TOGGLE_MUTE = 13;
    public static final byte BACK = 14;
    public static final byte HOME = 15;
    public static final byte NOTIFICATIONS = 16;
    public static final byte BRIEF_NOTIFICATIONS = 17;
    public static final byte RECENTS = 18;
    public static final byte LAUNCH_APP = 19;
    public static final List<Byte> GLOBAL_ACTIONS = Arrays.asList(BACK, HOME, NOTIFICATIONS, BRIEF_NOTIFICATIONS, RECENTS);

    private ButtonItem[] buttonItems;
    private boolean enabled = true;

    final Handler actionsDelayHandler = new Handler();


    AudioManager audioManager;
    PowerManager powerManager;

    byte steeringPin;
    int R1;
    SharedPreferences preferences;
    long startedAt;

    protected IOIO ioio;
    private AnalogInput in;

    @Override
    public void onCreate() {
        super.onCreate();

        startedAt = System.currentTimeMillis();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);

        startForeground(626, new NotificationCompat.Builder(this, createNotificationChannel((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE))).setOngoing(true)
                .setSmallIcon(R.drawable.app_icon_foreground)
                .setPriority(PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, SettingsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP), 0))
                .setContentText("Monitoring IOIO-OTG")
                .build());

        preferences = getApplicationContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        // In case user did something wrong with integer value in Expert mode
        try {
            steeringPin = Byte.parseByte(getResources().getStringArray(R.array.ioio_analog_pins)[Math.max(0, Math.min(preferences.getInt("ioioPin", 0), getResources().getStringArray(R.array.ioio_analog_pins).length - 1))]);
        } catch (Exception ignored) {
            steeringPin = Byte.parseByte(getResources().getStringArray(R.array.ioio_analog_pins)[0]);
        }
        // In case user did something wrong with integer value in Expert mode
        try {
            // Resistance cannot be negative
            R1 = Math.max(0, preferences.getInt("knownResistor", 0));
        } catch (Exception ignored) {
            R1 = 0;
        }
        buttonItems = new Gson().fromJson(preferences.getString("buttons", "[]"), ButtonItem[].class);

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".service_setup_and_test"));

        showDebugValue(getString(R.string.monitoring_since, startedAt), "startedAt");
    }

    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final byte emulateButton = intent.getByteExtra("emulateButton", (byte) -1);
                final String jsonButtons = intent.getStringExtra("buttons");
                final byte ioioPin = intent.getByteExtra("ioioPin", (byte) -1);
                final int knownResistor = intent.getIntExtra("knownResistor", -1);
                final boolean getStartedAt = intent.getBooleanExtra("getStartedAt", false);

                enabled = intent.getBooleanExtra("enable", true);

                if (getStartedAt) {
                    showDebugValue(getString(R.string.monitoring_since, startedAt), "startedAt");
                } else if (intent.getBooleanExtra("hardReset", false)) {
                    try {
                        ioio.hardReset();
                    } catch (Exception ignored) {
                    }
                } else if (ioioPin != -1) {
                    steeringPin = Byte.parseByte(getResources().getStringArray(R.array.ioio_analog_pins)[ioioPin]);

                    // IOIO-OTG night be disconnected. We do not wait for connection, as all of these things will be done upon connection
                    try {
                        ioio.beginBatch();
                        ioio.softReset();
                        in.close();
                        in = ioio.openAnalogInput(steeringPin);
                        ioio.endBatch();
                    } catch (Exception ignored) {
                    }
                } else if (knownResistor != -1) {
                    R1 = knownResistor;
                } else if (jsonButtons != null) {
                    // Save a working copy
                    buttonItems = new Gson().fromJson(jsonButtons, ButtonItem[].class);

                    // Centralize saving of buttons to the preferences
                    preferences.edit().putString("buttons", jsonButtons).apply();
                } else if (emulateButton != -1) {
                    final ButtonItem buttonItem = buttonItems[emulateButton];
                    final String type = intent.getStringExtra("type");

                    assert type != null;
                    if (type.equals("tap")) {
                        executeAction(buttonItem.getTapAction(), buttonItem.getTapActionSupplement());
                    } else {
                        executeAction(buttonItem.getHoldAction(), buttonItem.getHoldActionSupplement());
                    }
                }
            }
        }
    };

    private String createNotificationChannel(NotificationManager notificationManager) {
        final String channelId = "ioio-otg-head-unit-foreground-service";
        final NotificationChannel channel = new NotificationChannel(channelId, "IOIO-OTG Head Unit Foreground Service", NotificationManager.IMPORTANCE_HIGH);

        // Supposedly stops notification LED
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        notificationManager.createNotificationChannel(channel);

        return channelId;
    }

    @Override
    protected IOIOLooper createIOIOLooper() {
        return new BaseIOIOLooper() {
            @Override
            protected void setup() throws ConnectionLostException {
                // In case user did something wrong with boolean value in Expert mode
                try {
                    if (preferences.getBoolean("volumeSuppression", false) && checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") == PackageManager.PERMISSION_GRANTED) {
                        Settings.Global.putString(getContentResolver(), "audio_safe_volume_state", "2");
                    }
                } catch (Exception ignored) {
                }

                // Expose it so later we can reset it, or change input pin
                ioio = ioio_;
                in = ioio_.openAnalogInput(steeringPin);

                Runnable runnable = () -> {
                    // In case user did something wrong with boolean value in Expert mode
                    try {
                        if (preferences.getBoolean("autoPlay", false)) {
                            MediaButtonControl(KeyEvent.KEYCODE_MEDIA_PLAY);
                        }
                        if (preferences.getBoolean("autoUnmute", false) && audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
                            audioManager.adjustSuggestedStreamVolume(AudioManager.ADJUST_UNMUTE, AudioManager.STREAM_MUSIC, AudioManager.FLAG_PLAY_SOUND);
                        }
                    } catch (Exception ignored) {
                    }

                    // In case user did something wrong with boolean value in Expert mode
                    try {
                        // Ignore initial start. It happens not only when user is starting app
                        // manually, but also on app update, or app restart (through Expert Mode)
                        if (preferences.getBoolean("wakeupScreen", false) && (System.currentTimeMillis() - startedAt) > 10000) {
                            // Start splash screen activity, which should wakeup the screen
                            getApplicationContext().startActivity(
                                    new Intent(getApplicationContext(), SplashActivity.class)
                                            .setAction(Intent.ACTION_VIEW)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            );
                        }
                        // Still start the app if no splash screen and app launch set
                        else {
                            // In case user did something wrong with the string value in Expert mode
                            try {
                                String packageId = preferences.getString("appOnConnect", "");

                                if (!packageId.equals("")) {
                                    startActivity(getPackageManager().getLaunchIntentForPackage(packageId));
                                }
                            } catch (Exception ignored) {
                            }
                        }
                    } catch (Exception ignored) {
                    }

                    // Send feedback to the user
                    showDebugValue(getString(R.string.last_active_button, ""), "activeButton");
                };

                // In case user did something wrong with boolean value in Expert mode
                try {
                    byte timeout = 0;

                    // Stop delayed actions, if any
                    actionsDelayHandler.removeCallbacksAndMessages(null);

                    if (preferences.getBoolean("actionsDelay", false)) {
                        timeout = 5;
                    }

                    actionsDelayHandler.postDelayed(runnable, timeout * 1000);

                } catch (Exception ignored) {
                }
            }

            private int getResistance(final float Vout) {
                if (Vout > 0) {
                    return Math.round((3.3f / Vout - 1) * R1);
                }

                return Math.round(Vout);
            }

            private boolean isInRange(final int resistance, final ButtonItem buttonItem) {
                return buttonItem.getMin() < resistance && buttonItem.getMax() > resistance;
            }

            @Override
            public void loop() throws ConnectionLostException, InterruptedException {
                if (R1 > 0) {
                    int resistance = getResistance(in.getVoltage());

                    if (resistance > 0) {
                        showDebugValue(getString(R.string.measured_resistance_with_column, String.valueOf(resistance)), "resistance");

                        ButtonItem buttonInRange = null;

                        // We are disabled when in Auto detect mode
                        if (enabled) {
                            for (ButtonItem buttonItem : buttonItems) {
                                if (isInRange(resistance, buttonItem)) {
                                    buttonInRange = buttonItem;
                                    break;
                                }
                            }
                        }

                        if (buttonInRange != null) {
                            final byte tapAction = buttonInRange.getTapAction();
                            short keyPressTimeout = 800;

                            // Volume up and Volume down are special keys - they have special hold
                            // action. If user wants to assign additional buttons to these steering
                            // wheel buttons, then Volume Up/Down actions should be assigned as hold
                            // actions
                            if (tapAction == VOLUME_UP || tapAction == VOLUME_DOWN) {
                                // First execute action, then check if user is holding. This way
                                // user response is faster and code is cleaner
                                volumeAction(tapAction == VOLUME_UP ? AudioManager.ADJUST_RAISE : AudioManager.ADJUST_LOWER);

                                // Wait to see if being held
                                Thread.sleep(300);
                                resistance = getResistance(in.getVoltage());
                                showDebugValue(getString(R.string.last_active_button, getString(R.string.last_active_button_tap, buttonInRange.getName())), "activeButton");

                                // Check if being held
                                if (isInRange(resistance, buttonInRange)) {
                                    // This will immediately jump back to this special case if
                                    // condition
                                    keyPressTimeout = 0;
                                    showDebugValue(getString(R.string.last_active_button, getString(R.string.last_active_button_hold, buttonInRange.getName())), "activeButton");
                                }
                            } else {
                                // Before deciding what to do we need to first decide if this is a
                                // tap or hold, so wait a bit
                                Thread.sleep(300);
                                resistance = getResistance(in.getVoltage());

                                // If still in range, then it is a hold, otherwise a tap
                                if (isInRange(resistance, buttonInRange)) {
                                    final byte holdAction = buttonInRange.getHoldAction();

                                    if (holdAction != NO_ACTION) {
                                        showDebugValue(getString(R.string.last_active_button, getString(R.string.last_active_button_hold, buttonInRange.getName())), "activeButton");

                                        executeAction(holdAction, buttonInRange.getHoldActionSupplement());
                                    }
                                } else if (tapAction != NO_ACTION) {
                                    showDebugValue(getString(R.string.last_active_button, getString(R.string.last_active_button_tap, buttonInRange.getName())), "activeButton");

                                    executeAction(tapAction, buttonInRange.getTapActionSupplement());
                                }
                            }

                            Thread.sleep(keyPressTimeout);
                        } else {
                            Thread.sleep(200);
                        }
                    } else {
                        Thread.sleep(200);
                    }
                }
            }

            @Override
            public void disconnected() {
                // Stop delayed actions, if any
                actionsDelayHandler.removeCallbacksAndMessages(null);

                // In case user did something wrong with boolean value in Expert mode
                try {
                    if (preferences.getBoolean("volumeSuppression", false) && checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") == PackageManager.PERMISSION_GRANTED) {
                        Settings.Global.putString(getContentResolver(), "audio_safe_volume_state", "2");
                    }
                } catch (Exception ignored) {
                }

                // Don't drain battery
                MediaButtonControl(KeyEvent.KEYCODE_MEDIA_PAUSE);

                // This will be re-opened when IOIO-OTG gets re-connected
                in.close();

                // Add some user feedback
                showDebugValue(getString(R.string.measured_resistance_with_column, "").replace("\u2126", ""), "resistance");
                showDebugValue(getString(R.string.last_active_button, ""), "activeButton");

                // Turn off screen
                DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

                if (devicePolicyManager.isAdminActive(new ComponentName(getApplicationContext(), MyDeviceAdminReceiver.class))) {
                    // Even though setTurnScreenOn is not executed, screen turns of... turn it back off
                    devicePolicyManager.lockNow();
                }
            }
        };
    }

    private void showDebugValue(final String val, final String tag) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".resistance_report")
                        .putExtra("value", val)
                        .putExtra("tag", tag)
        );
    }

    private void wakeUpDisplay(final byte timeout) {
        getApplicationContext().startActivity(
                new Intent(getApplicationContext(), BriefWakeUpActivity.class)
                        .setAction(Intent.ACTION_VIEW)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("timeout", timeout)
        );
    }

    private void prolongBriefOn(final byte timeout) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(
                new Intent(BuildConfig.APPLICATION_ID + ".brief_wakeup_extend")
                        .putExtra("timeout", timeout)
        );
    }

    private void beep(final String preference, final int toneType) {
        // In case user did something wrong with boolean value in Expert mode
        try {
            if (preferences.getBoolean(preference, false)) {
                final ToneGenerator beeper = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

                beeper.startTone(toneType);

                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    beeper.stopTone();
                    beeper.release();
                }, 200);
            }
        } catch (Exception ignored) {
        }
    }

    private void executeAction(final byte action, String actionSupplement) {
        switch (action) {
            case PLAY:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_PLAY, (byte) 5, true);
                break;
            case PLAY_PAUSE:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, (byte) 5, true);
                break;
            case PAUSE:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_PAUSE, (byte) 5, true);
                break;
            case STOP:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_STOP, (byte) 5, true);
                break;
            case NEXT:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_NEXT, (byte) 10, true);

                // In case user did something wrong with boolean value in Expert mode
                try {
                    if (preferences.getBoolean("briefShowNotificationsTrackChange", false)) {
                        executeAction(BRIEF_NOTIFICATIONS, "5000");
                    }
                } catch (Exception ignored) {
                }
            case FORWARD:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_FAST_FORWARD, (byte) 5, false);
                break;
            case REWIND:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_REWIND, (byte) 5, false);
                break;
            case PREVIOUS:
                multimediaAction(KeyEvent.KEYCODE_MEDIA_PREVIOUS, (byte) 10, true);

                // In case user did something wrong with boolean value in Expert mode
                try {
                    if (preferences.getBoolean("briefShowNotificationsTrackChange", false)) {
                        executeAction(BRIEF_NOTIFICATIONS, "5000");
                    }
                } catch (Exception ignored) {
                }
                break;
            case VOLUME_UP:
                volumeAction(AudioManager.ADJUST_RAISE);
                break;
            case VOLUME_DOWN:
                volumeAction(AudioManager.ADJUST_LOWER);
                break;
            case MUTE:
                volumeAction(AudioManager.ADJUST_MUTE);
                break;
            case UNMUTE:
                volumeAction(AudioManager.ADJUST_UNMUTE);
                break;
            case TOGGLE_MUTE:
                volumeAction(AudioManager.ADJUST_TOGGLE_MUTE);
                break;
            case BACK:
            case HOME:
            case NOTIFICATIONS:
            case BRIEF_NOTIFICATIONS:
            case RECENTS:
                // Check for preference is inside
                beep("beepOnGlobalAction", ToneGenerator.TONE_PROP_BEEP);

                final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);

                lbm.sendBroadcast(
                        new Intent(BuildConfig.APPLICATION_ID + ".execute_global")
                                .putExtra("executeGlobal",
                                        action == BACK ? GLOBAL_ACTION_BACK :
                                                action == HOME ? GLOBAL_ACTION_HOME :
                                                        (action == NOTIFICATIONS || action == BRIEF_NOTIFICATIONS) ? GLOBAL_ACTION_NOTIFICATIONS :
                                                                GLOBAL_ACTION_RECENTS
                                )
                );

                // Brief notifications is not a standard Android global action, but a combination of
                // 2 standard global actions - show notifications, wait a bit, hit Back button
                if (action == BRIEF_NOTIFICATIONS) {
                    short delay = 3000;

                    if (!actionSupplement.equals("")) {
                        delay = Short.parseShort(actionSupplement);
                    }

                    new Handler(Looper.getMainLooper()).postDelayed(() -> lbm.sendBroadcast(
                            new Intent(BuildConfig.APPLICATION_ID + ".execute_global")
                                    .putExtra("executeGlobal", GLOBAL_ACTION_BACK)
                    ), delay);
                }
                break;
            case LAUNCH_APP:
                launchAppAction(actionSupplement);
                break;
        }
    }

    private void volumeAction(final int action) {
        // In case user did something wrong with boolean value in Expert mode
        try {
            if (preferences.getBoolean("briefWakeOnVolumeChange", false) && !powerManager.isInteractive()) {
                wakeUpDisplay((byte) 5);
            }
        } catch (Exception ignored) {
        }

        // Cast that we want to reset screen off timer. This will do nothing if display was not turned on in the if condition above
        prolongBriefOn((byte) 5);

        // In case user did something wrong with boolean value in Expert mode
        try {
            audioManager.adjustSuggestedStreamVolume(action, AudioManager.STREAM_MUSIC, preferences.getBoolean("showAudioVolume", false) ? AudioManager.FLAG_SHOW_UI : 0);
        } catch (Exception ignored) {
        }

        int toneType = ToneGenerator.TONE_PROP_BEEP;

        // In case user did something wrong with boolean value in Expert mode
        try {
            // getStreamVolume lags behind when called after adjustSuggestedStreamVolume, so if
            // volume was 90 and became 100, check here will fail. This is not terrible, but what is
            // terrible is the opposite - if volume was 100 and became 90, this check will succeed
            // and long beep will be heard by the user. That is why we check only for volume up and
            // do not count other volume changes
            if (action == AudioManager.ADJUST_RAISE && preferences.getBoolean("longBeepOnMaxVolume", false) && audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)) {
                toneType = ToneGenerator.TONE_PROP_PROMPT;
            }
        } catch (Exception ignored) {
        }

        // Check for preference is inside
        beep("beepOnVolumeChange", toneType);
    }

    private void launchAppAction(final String packageId) {
        if (!packageId.equals("")) {
            // Check for preference is inside
            beep("beepOnAppLaunch", ToneGenerator.TONE_PROP_BEEP);

            // When launching application we would like the screen to remain on
            if (!powerManager.isInteractive()) wakeUpDisplay((byte) 0);

            startActivity(getPackageManager().getLaunchIntentForPackage(packageId));
        }
    }

    private void multimediaAction(final int key, final byte time, final boolean tryBeep) {
        // In case user did something wrong with boolean value in Expert mode
        try {
            if (preferences.getBoolean("briefWakeOnTrackChange", false) && !powerManager.isInteractive()) {
                wakeUpDisplay(time);
            }
        } catch (Exception ignored) {
        }

        // Cast that we want to reset screen off timer. This will do nothing if display was not turned on in the if condition above
        prolongBriefOn(time);

        // Emulate button press
        MediaButtonControl(key);

        if (tryBeep) {
            beep("beepOnMultimediaAction", ToneGenerator.TONE_PROP_BEEP);
        }
    }

    private void MediaButtonControl(final int Keycode) {
        audioManager.dispatchMediaKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, Keycode));
        audioManager.dispatchMediaKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, Keycode));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
