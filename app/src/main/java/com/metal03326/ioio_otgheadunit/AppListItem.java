package com.metal03326.ioio_otgheadunit;

import android.graphics.drawable.Drawable;

public class AppListItem {
    private String id;
    private String name;
    private Drawable icon;

    public AppListItem() {
        id = "";
        name = "";
        icon = null;
    }

    public void setId(final String newId) {
        id = newId;
    }

    public String getId() {
        return id;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getName() {
        return name;
    }

    public void setIcon(final Drawable newIcon) {
        icon = newIcon;
    }

    public Drawable getIcon() {
        return icon;
    }
}
