package com.metal03326.ioio_otgheadunit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.Objects;

public class AutoDetectActivity extends AppCompatActivity {
    static final byte NEUTRAL = 0;
    static final byte ADD_DETECT = 1;
    static final byte ADD_NAME = 2;
    private byte currentScreen = NEUTRAL;
    private int neutralResistance = 0;
    // Hold last x amount of measures. Used to do average and to calculate countdown
    private int[] measures = {};
    private ButtonItem[] buttonItems = {};
    ViewGroup layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_detect);

        layout = findViewById(R.id.layout);

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".resistance_report"));

        setupScreen();
    }

    public void setupScreen() {
        final com.google.android.material.textfield.TextInputLayout buttonName = layout.findViewWithTag("buttonName");
        final TextView measuredResistance = layout.findViewWithTag("measuredResistance");

        layout.findViewWithTag("screen0").setVisibility(currentScreen == NEUTRAL ? View.VISIBLE : View.GONE);
        layout.findViewWithTag("screen1").setVisibility(currentScreen == ADD_DETECT ? View.VISIBLE : View.GONE);
        layout.findViewWithTag("screen2").setVisibility(currentScreen == ADD_NAME ? View.VISIBLE : View.GONE);

        // If we already have button items created, then we are rendering the last one, otherwise we
        // render neutral resistance
        if (buttonItems.length > 0) {
            final ButtonItem buttonItem = buttonItems[buttonItems.length - 1];

            measuredResistance.setText(String.valueOf(buttonItem.getMin()));

            buttonName.setVisibility(View.VISIBLE);
            buttonName.requestFocus();
            Objects.requireNonNull(buttonName.getEditText()).setText(buttonItem.getName());
        } else {
            measuredResistance.setText(String.valueOf(neutralResistance));

            // Neutral resistance doesn't have name
            buttonName.setVisibility(View.GONE);
        }
    }

    /**
     * Does not modify original
     * @return Array of button items that will replace the arrays in the settings and in the service
     */
    ButtonItem[] getResultingButtonItems() {
        // Deep copy buttonItems
        final Gson gson = new Gson();
        final ButtonItem[] resultingButtonItems = gson.fromJson(gson.toJson(buttonItems), ButtonItem[].class);

        // Sort by smallest resistance first
        Arrays.sort(resultingButtonItems, (b1, b2) -> b1.getMin() - b2.getMin());

        // Set min of this item and max on previous item to be in between (with 10 ohms margin)
        for (byte i = 0; i < resultingButtonItems.length; i++) {
            final ButtonItem buttonItem = resultingButtonItems[i];
            final int detectedValue = buttonItem.getMin();

            // No previous item, just slash detected value in half
            if (i == 0) {
                buttonItem.setMin(detectedValue / 2);
            } else {
                final ButtonItem previousButtonItem = resultingButtonItems[i - 1];
                final int median = (detectedValue + previousButtonItem.getMax()) / 2;

                buttonItem.setMin(median + 5);
                previousButtonItem.setMax(median - 5);
            }

            // No next item, use neutral resistance instead of min of next item
            if (i == resultingButtonItems.length - 1) {
                buttonItem.setMax((detectedValue + neutralResistance) / 2);
            }
        }

        return resultingButtonItems;
    }

    public void askAndFinish() {
        final StringBuilder buttons = new StringBuilder();
        final ButtonItem[] resultingButtonItems = getResultingButtonItems();

        for (ButtonItem buttonItem : resultingButtonItems) {
            buttons.append("\n").append(buttonItem.getName()).append(": ").append(buttonItem.getMin()).append("Ω - ").append(buttonItem.getMax()).append("Ω");
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.overwrite_existing_buttons)
                .setMessage(getString(R.string.overwrite_buttons_with) + buttons)
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                    setResult(Activity.RESULT_OK, new Intent().putExtra("result", new Gson().toJson(resultingButtonItems)));

                    finish();
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void onCancelButtonDetect(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        currentScreen = ADD_NAME;

        setupScreen();
    }

    public void onAddButtonClick(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        final int length = buttonItems.length;

        // Initial click of the button will not have name, as first button is about to get created
        if (length > 0) {
            final EditText field = layout.findViewWithTag("name");
            final String name = field.getText().toString().trim();

            if (!name.equals("")) {
                buttonItems[length - 1].setName(name);
            }

            field.setText("");
        }

        currentScreen = ADD_DETECT;

        setupScreen();
    }

    public void onFinishClick(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        buttonItems[buttonItems.length - 1].setName(((EditText) layout.findViewWithTag("name")).getText().toString().trim());

        askAndFinish();
    }

    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final String tag = intent.getStringExtra("tag");

                assert tag != null;
                if (tag.equals("resistance")) {
                    final int value = Integer.parseInt(intent.getStringExtra("value").replaceAll("[^0-9]", ""));

                    runOnUiThread(() -> {
                        // We need to detect first the neutral resistance
                        if (neutralResistance == 0) {
                            final View detectingSoon = layout.findViewWithTag("detectingSoon");
                            final View detectingNow = layout.findViewWithTag("detectingNow");
                            final TextView neutralCountdown = layout.findViewWithTag("neutralCountdown");
                            final int originalLength = measures.length;
                            final int newLength = originalLength + 1;
                            final int[] newMeasures = new int[newLength];

                            // We received signal from the foreground service, so IOIO-OTG is
                            // connected and we can remove the waiting text, and show countdown
                            layout.findViewWithTag("waiting").setVisibility(View.GONE);
                            neutralCountdown.setVisibility(View.VISIBLE);

                            // Increase size of array
                            System.arraycopy(measures, 0, newMeasures, 0, originalLength);

                            // Add measure as last item
                            newMeasures[originalLength] = value;

                            measures = newMeasures;
                            int step = newLength / 15;

                            // Measures never get reset, they keep counting.
                            // step == 0 we are showing soon screen
                            // step == 1 we are showing detecting screen
                            // step == 2 we average the values from 15 to 30 - that is our neutral
                            // resistance
                            if (step == 2) {
                                int total = 0;

                                for (int i = 15; i < measures.length; i++) {
                                    total += measures[i];
                                }

                                neutralResistance = total / 15;

                                // We are done with all measures, reset for next time
                                measures = new int[0];

                                // Show result to the user
                                currentScreen = ADD_NAME;
                                setupScreen();
                            } else {
                                final boolean isSoon = step == 0;

                                neutralCountdown.setText(String.valueOf((int) (3 - Math.floor((float)newLength % 15 / 5))));

                                detectingSoon.setVisibility(isSoon ? View.VISIBLE : View.GONE);
                                detectingNow.setVisibility(isSoon ? View.GONE : View.VISIBLE);
                            }
                        } else if (neutralResistance * 0.9 > value && currentScreen == ADD_DETECT) {
                            final View detectingHint = layout.findViewWithTag("holdAndWaitHint");
                            final View detectingNow = layout.findViewWithTag("detectingButtonNow");
                            final TextView detectingCountdown = layout.findViewWithTag("detectingCountdown");
                            final int originalLength = measures.length;
                            final int newLength = originalLength + 1;
                            final int[] newMeasures = new int[newLength];
                            final boolean isDone = newLength == 15;

                            detectingCountdown.setVisibility(View.VISIBLE);

                            System.arraycopy(measures, 0, newMeasures, 0, originalLength);

                            newMeasures[originalLength] = value;

                            measures = newMeasures;

                            // Show progress
                            detectingCountdown.setText(isDone ? "3" : String.valueOf((int) (3 - Math.floor((float) newLength / 5))));
                            detectingCountdown.setVisibility(isDone ? View.GONE : View.VISIBLE);
                            detectingHint.setVisibility(isDone ? View.VISIBLE : View.GONE);
                            detectingNow.setVisibility(isDone ? View.GONE : View.VISIBLE);

                            if (isDone) {
                                int total = 0;

                                for (int measure : measures) {
                                    total += measure;
                                }

                                // Get average of all readings
                                final int resistance = total / measures.length;

                                // We are done with all measures, reset for next time
                                measures = new int[0];

                                // Set measured resistance to the last button item
                                buttonItems = Arrays.copyOf(buttonItems, buttonItems.length + 1);
                                buttonItems[buttonItems.length - 1] = new ButtonItem().setMin(resistance).setMax(resistance);

                                // Show result to the user
                                currentScreen = ADD_NAME;
                                setupScreen();
                            }
                        }
                    });
                }
            }
        }
    };
}