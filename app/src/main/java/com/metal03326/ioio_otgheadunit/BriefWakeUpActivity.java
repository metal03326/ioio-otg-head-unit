package com.metal03326.ioio_otgheadunit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

public class BriefWakeUpActivity extends AppCompatActivity {
    final Handler handler = new Handler();
    DevicePolicyManager deviceManger;
    Runnable runnable;

    public void setNewTimer(final byte timeout) {
        handler.removeCallbacksAndMessages(null);

        if (timeout > 0) {
            handler.postDelayed(runnable, timeout * 1000);
        }
    }

    public void noDeviceLock(@SuppressWarnings({"unused", "RedundantSuppression"}) final View view) {
        setNewTimer((byte) 0);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        deviceManger = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        byte timeout = getIntent().getExtras().getByte("timeout");

        setContentView(R.layout.activity_brief_wake_up);

        setShowWhenLocked(true);
        setTurnScreenOn(true);

        // When launching app we do not turn off the screen, but when changing volume we do
        if (timeout > 0) {
            runnable = () -> {
                if (deviceManger.isAdminActive(new ComponentName(this, MyDeviceAdminReceiver.class))) {
                    deviceManger.lockNow();
                }

                finish();
            };

            handler.postDelayed(runnable, timeout * 1000);
        } else {
            finish();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BuildConfig.APPLICATION_ID + ".brief_wakeup_extend"));
    }

    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                setNewTimer(intent.getByteExtra("timeout", (byte) 0));
            }
        }
    };
}
