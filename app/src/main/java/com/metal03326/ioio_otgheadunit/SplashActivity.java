package com.metal03326.ioio_otgheadunit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences preferences = getApplicationContext().getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        final String splashImageStr = preferences.getString("splashImage", "");
        byte splashVisibleFor = 0;
        // In case user did something wrong with byte value in Expert mode
        try {
            // Time cannot be negative
            splashVisibleFor = (byte) Math.max(0, preferences.getInt("splashVisibleFor", 0));
        } catch (Exception ignored) {
        }

        setShowWhenLocked(true);
        setTurnScreenOn(true);

        // No image or no time = no splash screen
        if (splashImageStr.equals("") || splashVisibleFor == 0) {
            startApp(preferences);

            finish();
        }

        setContentView(R.layout.activity_splash);

        pl.droidsonroids.gif.GifImageView splashImage = findViewById(R.id.animatedSplashImage);

        // Set the image to the GifImageView
        if (Build.VERSION.SDK_INT >= 29) {
            MediaScannerConnection.scanFile(this,
                    new String[]{splashImageStr}, null,
                    (path, uri) -> splashImage.setImageURI(uri));
        } else {
            splashImage.setImageURI(Uri.parse("file://" + splashImageStr));
        }

        // Hide splash screen after user adjusted timeout
        new Handler().postDelayed(() -> {
            startApp(preferences);

            finish();
        }, splashVisibleFor * 1000);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE
        );
    }

    private void startApp(final SharedPreferences preferences) {
        boolean test = false;

        try {
            test = getIntent().getExtras().getBoolean("test");
        } catch (Exception ignored) {
        }

        if (!test) {
            // In case user did something wrong with the string value in Expert mode
            try {
                String packageId = preferences.getString("appOnConnect", "");

                if (!packageId.equals("")) {
                    startActivity(getPackageManager().getLaunchIntentForPackage(packageId));
                }
            } catch (Exception ignored) {
            }
        }
    }
}
