[<img src="en_get.svg" width="200">](https://play.google.com/store/apps/details?id=com.metal03326.ioio_otgheadunit)

## Features:
- Control Android device through steering wheel buttons (IOIO-OTG board required).
- Autodetect function of pressed steering wheel button.
- Automatic screen off when ACC goes off (Device Admin rights required).
- Show image (animated GIF supported) when ACC comes back on (Storage access required).
- Assign up to 2 actions per steering wheel button - one for press and one for hold.
- Ability to execute multimedia action (Play/Pause, Next, Previous, etc.) on steering wheel button press or hold.
- Ability to execute audio change action (Volume Up/Down, Mute, etc.) on steering wheel button press or hold.
- Ability to execute global actions (Back, Home, Recents. Requires accessibility service) on steering wheel button press or hold.
- Ability to launch installed app on steering wheel button press or hold.
- Option to hide status and/or navigation bars (system-wide immersive mode) on up to Android 10 (included). Requires granting permissions through adb.
- Other small tweaks.

This app does **NOT** require ROOT, but it'll require other permissions to make features work. All permissions are mandatory **ONLY** if you want that particular feature:
- **Storage access**: This is needed to select the image for the splash screen that appears when ACC comes back on. Nothing else. If not granted, no splash image will be shown.
- **Device Admin**: This is needed to lock the device, which turns off the screen. Nothing else. If not enabled, the device won't turn off the screen when ACC goes off and will always turn on the screen when ACC comes back on, regardless of the chosen option within the app.
- **Accessibility service**: This is needed to execute global actions - Back, Home, Show notifications and Show recent apps. Nothing else. If not enabled, those actions will not work.
- **Write secure settings**: This is needed to make status and/or navigation bars disappear, so the device looks more like a head unit. Unfortunately, support for that dropped in Android 11 and to enable it you'll need to grant permissions through ADB (search on the internet how to connect your particular device through ADB) by executing ```adb shell pm grant com.metal03326.ioio_otgheadunit android.permission.WRITE_SECURE_SETTINGS```

## Tested on:
###Real hardware (with IOIO-OTG attached):
- Xiaomi Redmi Go (Android 8.1 Go Edition) - still running in my Mazda 626 GF which uses steering wheel from Mazda 6 GG
- Samsung Galaxy Xcover 4 (Android 9) - this was only for the test
###Emulators:
- Android 9
- Android 10
- Android 11

I do not have lots of hardware to test with, so bugs are expected. Please open an issue, and I'll do my best to resolve it.

## Hardware

### Phone and issues
Main phone I've used is **Xiaomi Redmi Go (Android 8.1 Go Edition)**, but all phones will have the same issue: battery **OVERHEATING** or **FREEZING**. The way to solve that is to replace the battery **cells** with **buck converter** and build-in thermistor (on the cable of the battery) with fixed resistor. The **Buck converter** should grab power from a constant 12V (I use dome lights as a source) and go through a fuse (**ROOM 10A** in my case).

### Rearview camera
This setup is external for the IOIO-OTG, but I'll cover it here as well. It all revolves around the fact that Android will automatically launch an app when particular USB device is plugged in (in that case USB reader for AV camera). Setup proper app and make USB plugging-in to be automatic by grabbing reverse gear signal and providing power to both the camera and the reader. In addition, I've also added manual trigger to trigger the camera without being on reverse gear. Check schematics for more details.

### Schematics

![](schematics/Connection%20Overview.png)

## Notes
- Schematics have been done with [Circuit Diagram](https://www.circuit-diagram.org/). Check [schematics](schematics) folder.
- Diodes seen without text are regular diodes, whatever you have lying around. I use FR104 (TH) and S1B (SMD) because I have heaps of them.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
